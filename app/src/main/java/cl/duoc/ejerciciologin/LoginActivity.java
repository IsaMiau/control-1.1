package cl.duoc.ejerciciologin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private Button btnEntrar, btnRegistro;
    private EditText etUsuario, etPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario = (EditText) findViewById(R.id.etUser);
        etPass = (EditText) findViewById(R.id.etPass);

        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mensaje = "";

                if(!etUsuario.getText().toString().equals("admin")){
                    mensaje += "Usuario incorrecto \n";
                }
                if(!etPass.getText().toString().equals("1234")){
                    mensaje += "Password incorrecta \n";
                }
                if(mensaje.length() > 1){
                    Toast.makeText(LoginActivity.this, mensaje, Toast.LENGTH_LONG).show();
                    mensaje = "";
                }
                else{
                    mensaje = "Entró con éxito";
                    Toast.makeText(LoginActivity.this, mensaje, Toast.LENGTH_LONG).show();
                }

            }
        });

        btnRegistro = (Button) findViewById(R.id.btnRegistro);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });
    }
}
