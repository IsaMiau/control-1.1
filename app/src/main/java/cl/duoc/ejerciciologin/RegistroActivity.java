package cl.duoc.ejerciciologin;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

public class RegistroActivity extends AppCompatActivity {

    private EditText etUser, etNombres, etApellidos, etRut, etEdad,  etPass, etPass2;
    private Button btnRegistro, etFecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        etUser = (EditText) findViewById(R.id.etUser);
        etNombres = (EditText) findViewById(R.id.etNombres);
        etApellidos = (EditText) findViewById(R.id.etApellidos);
        etRut = (EditText) findViewById(R.id.etRut);
        etEdad = (EditText) findViewById(R.id.etEdad);
        etFecha = (Button) findViewById(R.id.etFecha);
        etPass = (EditText) findViewById(R.id.etPass);
        etPass2 = (EditText) findViewById(R.id.etPass2);
        btnRegistro = (Button) findViewById(R.id.btnRegistro);

        etFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mensaje = "";

                if(etUser.getText().toString().equals("admin")){
                    mensaje += "El usuario ya existe \n";
                    Toast.makeText(RegistroActivity.this, mensaje, Toast.LENGTH_LONG).show();
                }
                if(etUser.getText().toString().length() < 1){
                    mensaje += "Ingrese nombre de usuario \n";
                }
                if(etNombres.getText().toString().length() < 1){
                    mensaje += "Ingrese nombres \n";
                }
                if(etApellidos.getText().toString().length() < 1){
                    mensaje += "Ingrese apellidos \n";
                }
                if(etRut.getText().toString().length() < 1){
                    mensaje += "Ingrese RUT \n";
                }
                if(etEdad.getText().toString().length() < 1){
                    mensaje += "Ingrese edad \n";
                }
                if(etFecha.getText().toString().length() < 1){
                    mensaje += "Ingrese fecha nacimiento \n";
                }
                if(etPass.getText().toString().length() < 1){
                    mensaje += "Ingrese clave \n";
                }if(etPass2.getText().toString().length() < 1){
                    mensaje += "Debe repetir clave \n";
                }if(!etPass.getText().toString().equals(etPass2.getText().toString())){
                    mensaje += "Las contraseñas deben coincidir \n";
                    Toast.makeText(RegistroActivity.this, mensaje, Toast.LENGTH_LONG).show();
                }
                if(mensaje.length() > 1){
                    Toast.makeText(RegistroActivity.this, mensaje, Toast.LENGTH_LONG).show();
                }
                if(mensaje.length() == 0){
                    mensaje ="Registro Exitoso";
                    Toast.makeText(RegistroActivity.this, mensaje, Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(RegistroActivity.this, LoginActivity.class);
                    startActivity(i);
                }

            }
        });

    }

    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etFecha.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }
}
